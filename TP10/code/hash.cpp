#ifdef OSX
#include <GLUT/glut.h>
#elif LINUX
#include <GL/glut.h>
#endif
#include <iostream>
#include <string>
#include <set>
#include <utility>
#include <random>
#include <fstream>
#include <boost/functional/hash.hpp>

using namespace std;

set<string> words;
GLint* points;
int size = 0;
set<string> pre_hached_table ;


void init(float r, float g, float b)
{
	glClearColor(r,g,b,0.0);  
	glMatrixMode (GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D (1.0, 128.0, 1.0, 128.0);
}

unsigned long long pre_hach(string word){
	//cout << "Word : " << word ;
	unsigned long long res = 0;
	string pre_hached ;
	
	int len = word.length() ;
	int i ;
	if(len<=9) {
		res = 1 ;
		for(i=0; i<len; i++){
			res += word[i] << 7*i ;
			//res += (unsigned long long) word[i] * (unsigned long long) pow(128, i) ;
		}
	}
	else res = len ;
	//cout <<" => pre-hach : " << res << endl ;
	return res ;
}

pair<int,int> hash_function(string word)
{
	
	int x, y, m;
	unsigned long long hash, prehash ;
	pair<int,int> coord;

	// Prehashing
	prehash = pre_hach(word) ;

	// Methode de division : choix de m 
	m = pow(2, 14);
	hash = prehash % m ;

	x = hash% (unsigned long long)pow(2,7) ;
	y = (unsigned long long)(hash >> 7)% (unsigned long long)pow(2,7);

	/* boost::hash<string> string_hash ;
	hash = string_hash(word) ;

	x = hash% (unsigned long long)pow(2,7) ;
	y =  (unsigned long long)(hash >> 7)% (unsigned long long)pow(2,7); */
	coord = make_pair(x,y);
	return coord;
}

void make_points()
{
	pair<int,int> coord;
	int i=0;
	for (set<string>::iterator it=words.begin(); it!=words.end(); ++it)
	{
		coord = hash_function(*it);
		points[i++] = (GLint)coord.first;
		points[i++] = (GLint)coord.second;	
	}
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT);
	glColor3f(1.0, 0.0, 0.0);
	glViewport(128.0,128.0,512.0,512.0);
	glPointSize(2.0f);
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(2, GL_INT, 0, points);
	glDrawArrays(GL_POINTS,0,size);
	glDisableClientState(GL_VERTEX_ARRAY);
	glutSwapBuffers();
}

int main(int argc,char *argv[])
{
	
	string word;

	if ( argc != 2 )
	{
		cout << "Enter a file name." << endl;
	}
	else
	{
		ifstream file (argv[1]);
		
		string str;
		while ( file >> word )
		{
			words.insert(word);
		}
		size = words.size();
		cout << "Document size: " << words.size() << " words" << endl;

		points = (GLint *)malloc(2*size*sizeof(GLint));
		make_points();
		glutInit(&argc,argv);
		glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB);
		glutInitWindowSize (768, 768);
		glutInitWindowPosition (200, 200);
		glutCreateWindow ("Hash Function Visualization");
		init(0.0,0.0,0.0);
		glutDisplayFunc(display);
		glutMainLoop();
	}
	return 0;
}
