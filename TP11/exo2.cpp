#include <stdio.h>
#include <iostream>
#include <vector>
using namespace std ;
vector<double> values = {7,6,5,4,3,1,0.5,0.1} ;
int n = values.size() ;

int cb_pieces(int montant){
    if(montant < 0){
        return -1000 ;
    }
    else if(montant == 0) return 0 ;
    else
    {
        int i = 0 ;
        // version liste décroissante
        while(i<n-1 && values[i]>montant ) i++;

        return 1+cb_pieces(montant-values[i]); 
        
    }
    
    
    return 0 ;
}

int main(){
    int val = 81;
    cout << cb_pieces(val) << endl ;

    return 0 ;
}