#include <stdio.h>
#include <iostream>
#include <vector>
using namespace std ;

vector<double> values = {0.1, 0.5, 1, 3, 4} ;
int n = values.size() ;
int cb_pieces(int montant){
    
    if(montant < 0){
        return -1 ;
    }
    else if(montant == 0) return 0 ;
    else
    {   int N = 10000 ;
        int i = 0 ;
        int j = 0 ;
        while(i<n-1 && values[i+1] <= montant ) {
            i++;
            int score = cb_pieces(montant-values[i])+1 ;
            if( score < N) {
                j = i ;
                N = score ;
            }
        }
        return 1+cb_pieces(montant-values[j]); 
        
    }
    
    
    return 0 ;
}

int main(){
    int val = 20;
    cout << cb_pieces(val) << endl ;

    return 0 ;
}