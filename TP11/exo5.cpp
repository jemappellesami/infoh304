#include <stdio.h>
#include <iostream>
#include <vector>
#include <map>
using namespace std ;

map<int, int> memoized ;
int Mem[100] ;
int ntot[100] ;

vector<double> values = {0.1, 0.5, 1, 3, 4,5,6,7} ;
int n = values.size() ;
int cb_pieces(int montant){
    map<int,int>::iterator itr = memoized.find(montant) ;
    
    if(montant < 0){
        return -1 ;
    }
    else if(montant == 0) return 0 ;
    //else if(*itr != NULL) return itr->second ;
    else if(Mem[montant]) return ntot[montant] ;
    else
    {   int N = 10000 ;
        int i = 0 ;
        int j = 0 ;
        while(i<n-1 && values[i+1] <= montant ) {
            i++;
            int score = cb_pieces(montant-values[i])+1 ;
            if( score < N) {
                j = i ;
                N = score ;
            }
        }
        int ret_val = 1+cb_pieces(montant-values[j]) ;
        memoized.insert(pair<int,int>(montant, ret_val)) ;
        Mem[montant] = 1 ;
        ntot[montant] = ret_val ;
        return ret_val; 
        
    }
    
    
    return 0 ;
}

int main(){
    int val = 81;
    cout << cb_pieces(val) << endl ;

    return 0 ;
}