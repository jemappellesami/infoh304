#include <iostream>
#include <string>
#include <chrono>
#include <thread>
#include <random>
#include <unordered_set>
extern "C" {
#include <UbigraphAPI.h>
}

using namespace std;

class BinarySearchTree
{
private:
	struct node
	{
		node *left;
		node *right;
		node *parent;
		int content;
	};
	node* root;
	node* tree_search(int content);
	node* tree_search(int content, node* location);
	void delete_no_child(node* location);
	void delete_left_child(node* location);
	void delete_right_child(node* location);
	void delete_two_children(node* location);
	
public:
	BinarySearchTree ()
	{
		root=NULL;
	}
	bool isEmpty()
	{
		return root==NULL;
	}
	void insert_element(int content);
	void delete_element(int content);
	void inorder(node* location);
	void print_inorder();
};


void BinarySearchTree::insert_element(int content)
{
	//New node
	node* n = new node();
	n->content = content;
	n->left = NULL;
	n->right = NULL;
	n->parent = NULL;
	
	//For visualization
	int eid,vid;
	//this_thread::sleep_for(chrono::milliseconds(100));
	ubigraph_new_vertex_w_id(content);
	ubigraph_set_vertex_attribute(content, "color", "#0000ff");
	ubigraph_set_vertex_attribute(content, "label", to_string(content).c_str());
	
	if(isEmpty())
	{
		root = n;
		ubigraph_set_vertex_attribute(content, "color", "#ff0000");
		//this_thread::sleep_for(chrono::milliseconds(100));
		ubigraph_set_vertex_attribute(content, "color", "#0000ff");
		//this_thread::sleep_for(chrono::milliseconds(100));
	}
	else
	{
		
		node* pointer = root;
		
		while ( pointer != NULL )
		{
			n->parent = pointer;
			
			if(n->content > pointer->content)
			{
				vid = pointer->content;
				ubigraph_set_vertex_attribute(vid, "color", "#ff0000");
				//this_thread::sleep_for(chrono::milliseconds(100));
				ubigraph_set_vertex_attribute(vid, "color", "#0000ff");
				//this_thread::sleep_for(chrono::milliseconds(100));
				
				pointer = pointer->right;
				
			}
			else
			{
				vid = pointer->content;
				ubigraph_set_vertex_attribute(vid, "color", "#ff0000");
				//this_thread::sleep_for(chrono::milliseconds(100));
				ubigraph_set_vertex_attribute(vid, "color", "#0000ff");
				//this_thread::sleep_for(chrono::milliseconds(100));
				
				pointer = pointer->left;
				
			}
		}
		
		if ( n->content < n->parent->content )
		{
			n->parent->left = n;
			//this_thread::sleep_for(chrono::milliseconds(200));
			eid = ubigraph_new_edge(n->parent->content, content);
			ubigraph_set_edge_attribute(eid, "oriented", "true");
			
		}
		else
		{
			n->parent->right = n;
			//this_thread::sleep_for(chrono::milliseconds(200));
			eid = ubigraph_new_edge(n->parent->content, content);
			ubigraph_set_edge_attribute(eid, "oriented", "true");		
		}
	}
}

void BinarySearchTree::inorder(node* location)
{
		if(location != NULL){
			inorder(location->left) ;
			cout<< location->content << ", " ; ;
			inorder(location->right) ;
		}
		
}

void BinarySearchTree::print_inorder()
{
	cout << "Contenu de l'arbre :";
	inorder(root);
	cout << endl;
}

BinarySearchTree::node* BinarySearchTree::tree_search(int content)
{	
	return tree_search(content, root);
}

BinarySearchTree::node* BinarySearchTree::tree_search(int content, node* location)
{	
	cout << location->content <<" " << content <<endl ;
	if(location->content == content){
		return location ;
	}
	else if(location->content < content){
		return tree_search(content, location->right) ;
	}
	else if(location->content > content){
		return tree_search(content, location->left) ;
	}
	return NULL;
}

void BinarySearchTree::delete_no_child(node* location)
{
	if(location == root){
		root = NULL ;
		
		ubigraph_remove_vertex(location->content) ;
		delete location ;
	}
	else if (location->parent->left != NULL && location->parent->left == location)
	{
		location->parent->left = NULL ;
		ubigraph_set_vertex_attribute(location->content, "color", "#00ff00") ;
		this_thread::sleep_for(chrono::milliseconds(2000)) ;
		ubigraph_remove_vertex(location->content) ;
		delete location ;
	}

	else if(location->parent->right !=NULL && location->parent->right == location){
		location->parent->right = NULL ;
		ubigraph_set_vertex_attribute(location->content, "color", "#00ff00") ;
		this_thread::sleep_for(chrono::milliseconds(2000)) ;
		ubigraph_remove_vertex(location->content) ;
		delete location ;
		
	}
	return ;
	
	/* cout << "L'élément à supprimer (" << location->content << ")  n'a pas d'enfant. Son noeud sera supprimé" << endl ;
	delete(location) ;
	location = NULL ; */
}

void BinarySearchTree::delete_left_child(node* location)
{
	int eid ;
	if(location == root){
		root = location->left ;
	}
	else if(location->parent->left != NULL && location->parent->left == location){
		location->parent->left = location->left ;
		location->left->parent = location->parent ;

		ubigraph_set_vertex_attribute(location->content, "color", "#00ff00") ;
		this_thread::sleep_for(chrono::milliseconds(2000)) ;
		ubigraph_remove_vertex(location->content) ;
		eid = ubigraph_new_edge(location->content, location->left->content) ;
		ubigraph_set_edge_attribute(eid, "oriented", "true") ;
		delete location ;
	}
	
	else if(location->parent->right != NULL && location->parent->right == location){
		location->parent->right = location->right ;
		location->right->parent = location->parent ;

		ubigraph_set_vertex_attribute(location->content, "color", "#00ff00") ;
		this_thread::sleep_for(chrono::milliseconds(2000)) ;
		ubigraph_remove_vertex(location->content) ;
		eid = ubigraph_new_edge(location->content, location->right->content) ;
		ubigraph_set_edge_attribute(eid, "oriented", "true") ;
		delete location ;
	}
	
	return ;
	/* cout << "L'élément à supprimer (" << location->content << ")  a un enfant à gauche (" << (location->left)->content << "). Son noeud sera remplacé par son fils unique" << endl ;
	node * temp, *tempP ;
	temp = location->left ;
	tempP = location->parent ;
	delete location ;
	location = temp ;
	cout << "Nous avons maintenant l'arborescence suivante. " << endl ;
	if(location->left != NULL){
		cout << "Son enfant de gauche est l'élément de clé " << (location->left)->content << endl ;
	}
	if(location->right != NULL){
		cout << "Son enfant de droite est l'élément de clé " << (location->right)->content << endl ;
	}
	if(location->parent != NULL){
		cout << "Son parent est l'élément de clé " << (location->parent)->content << endl ;
	} */
}

void BinarySearchTree::delete_right_child(node* location)
{
int eid ;
	if(location == root){
		root = location->right ;
	}
	else if(location->parent->right != NULL && location->parent->right == location){
		location->parent->right = location->right ;
		location->right->parent = location->parent ;

		ubigraph_set_vertex_attribute(location->content, "color", "#00ff00") ;
		this_thread::sleep_for(chrono::milliseconds(2000)) ;
		ubigraph_remove_vertex(location->content) ;
		eid = ubigraph_new_edge(location->content, location->left->content) ;
		ubigraph_set_edge_attribute(eid, "oriented", "true") ;
		delete location ;
	}
	
	else if(location->parent->left != NULL && location->parent->left == location){
		location->parent->left = location->left ;
		location->left->parent = location->parent ;

		ubigraph_set_vertex_attribute(location->content, "color", "#00ff00") ;
		this_thread::sleep_for(chrono::milliseconds(2000)) ;
		ubigraph_remove_vertex(location->content) ;
		eid = ubigraph_new_edge(location->content, location->left->content) ;
		ubigraph_set_edge_attribute(eid, "oriented", "true") ;
		delete location ;
	}
	//cout << "L'élément à supprimer (" << location->content << ")  a un enfant à droite (" << (location->right)->content << "). Son noeud sera remplacé par son fils unique" << endl ;
	//location = location->right ;
}

void BinarySearchTree::delete_two_children(node* location)
{
	node* successor = location->right ;
	node* left_child = successor->left ;
	while(left_child != NULL){
		successor = left_child;
		left_child = left_child->left ;
	}

	int new_content = successor -> content ;
	int old_content = location -> content ;

	if(successor->right == NULL){
		delete_no_child(successor) ;
	}
	else{
		delete_right_child(successor) ;
	}
	location->content = new_content ;

	ubigraph_remove_vertex(old_content) ;
	ubigraph_new_vertex_w_id(new_content) ;
	ubigraph_set_vertex_attribute(new_content, "color", "#0000ff") ;
	ubigraph_set_vertex_attribute(new_content, "label", to_string(new_content).c_str()) ;
	//cout << "L'élément à supprimer (" << location->content << ")  a deux enfants(" << (location->right)->content << ", " << (location->left)->content<<"). Son noeud sera remplacé par sucesseur" << endl ;
	return ;
}

void BinarySearchTree::delete_element(int content)
{
	node* location = tree_search(content) ;
	cout << "L'élement à supprimer a été trouvé. \nIl s'agit de l'élément de clé " << location->content<< endl ;
	if(location->left != NULL){
		cout << "Son enfant de gauche est l'élément de clé " << (location->left)->content << endl ;
	}
	if(location->right != NULL){
		cout << "Son enfant de droite est l'élément de clé " << (location->right)->content << endl ;
	}
	if(location->parent != NULL){
		cout << "Son parent est l'élément de clé " << (location->parent)->content << endl ;
	}
	if(location->left == NULL && location->right == NULL) delete_no_child(location) ;
	else if(location->left == NULL && location->right != NULL) delete_right_child(location) ;
	else if(location->left != NULL && location->right == NULL) delete_left_child(location) ;
	else if(location->left != NULL && location->right != NULL) delete_two_children(location) ;

}


int main()
{
	ubigraph_clear();
	unordered_set<int> vertices;
	int vertex;
	BinarySearchTree bst;
	
	random_device rd;
	mt19937 gen(rd());
	uniform_int_distribution<int> r(0, 1000);
	int n = 20 ;
	BinarySearchTree old_bst;

	//for(int i = 1; i<=10; i++) bst.insert_element(i) ;
	for ( int i=1; i<=n; i++ )
	{
		vertex = r(gen);
		if ( vertices.count(vertex) == 0 )
		{
			vertices.insert(vertex);
			bst.insert_element(vertex);
		}
		else
			i--;
	}	
	/* int i = 0 ;
	while(i<10){
		cin >> vertex ;
		bst.insert_element(vertex) ;
		i++ ;
		
	} */
	bst.insert_element(666) ;
	bst.insert_element(667) ;
	for ( int i=1; i<=n; i++ )
	{
		vertex = r(gen);
		if ( vertices.count(vertex) == 0 )
		{
			vertices.insert(vertex);
			bst.insert_element(vertex);
		}
		else
			i--;
	}	
	int eid ;
	bst.print_inorder() ;
	bst.delete_element(667) ;	

	return 0;
}
