#include <iostream>
#include <fstream>
#include <set>
using namespace std ;

void printSet(const set<string> & c, ostream & out = cout){
    set<string>::const_iterator itr ;
    for(itr = c.begin(); itr!=c.end(); ++itr){
        out << *itr << endl ;
    }
}

int main(int argc, char *argv[]){
    set<string> ensemble ;
    ifstream finput(argv[1]);
        if(finput.is_open()){
            string mot ;
            while(finput >> mot){
                ensemble.insert(mot) ;
            }
            finput.close() ;
            printSet(ensemble);
            cout << "Nombre de mots uniques : " << ensemble.size() << endl ;
        }
    else{
        cout << "Impossible d'ouvrir le fichier" << endl ;
        return 1 ;
    }
}
