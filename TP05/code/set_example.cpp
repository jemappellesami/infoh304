#include <iostream>
#include <string>
#include <set>
using namespace std;

int main(int argc, char *argv[])
{
	pair<set<string>::iterator,bool> ret;
	set<string> ensemble;
	//On essaye d'insérer le mot "test".
	ret=ensemble.insert("test");
	//apres l'insertion, ret vaut (itérateur vers "test",true).
	cout << "ret.first pointe vers le mot: " << *(ret.first) << endl;
	cout << "ret.second vaut: " << ret.second << endl;
	//On essaye de réinsérer le mot "test".
	ret=ensemble.insert("test");
	//Cette fois, ret vaut (itérateur vers "test",false).
	cout << "ret.first pointe vers le mot: " << *(ret.first) << endl;
	cout << "ret.second vaut: " << ret.second << endl;
	cout << "ret.second vaut: " << ret.second << endl;
}