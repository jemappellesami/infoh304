#include <iostream>
#include <fstream>
#include <map>
using namespace std ;

void printMap(const map<string, int> & c, ostream & out = cout){
    map<string, int>::const_iterator itr ;
    for(itr = c.begin() ; itr != c.end() ; ++itr){
        out << itr->first <<" " <<  itr->second << endl ;
    }
}

int main( int argc, char *argv[]){
    map<string, int> dictionnaire ;
    ifstream finput(argv[1]) ;
    if (finput.is_open()){
        string mot ;
        while(finput >> mot){
            int n = 1 ;
            pair<map<string,int>::iterator, bool> ret ;
            ret = dictionnaire.insert(pair<string, int>(mot, n)) ;
            if (ret.second == false){
                *(ret.first) ++ ;
            }
        }
        finput.close() ;
    }
    else{
        cout << "Fichier impossible à ouvrir zzz" << endl ;
        return 1 ;
    }
    ofstream foutput(argv[2]) ;
    if(foutput.is_open()){
        printMap(dictionnaire, foutput) ;
        foutput.close() ;
        return 0 ;
    }
    else{
        cout << "impossible d'ouvrir le fichier output" ;
        return 1 ;
    }


}